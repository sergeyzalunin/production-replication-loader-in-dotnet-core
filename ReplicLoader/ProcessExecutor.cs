using System;
using System.Diagnostics;
using System.IO;
using ReplicLoader.CommandLine;
using ReplicLoader.Logging;

namespace ReplicLoader
{
    internal class ProcessExecutor
    {
        private Log logger = null;
        private string stdout, stderr;
        private object locker = new object();
        public string PathToAdminToolsConsole { get; private set;}
        public string PathToCompilationPlugin { get; private set; }

        public ProcessExecutor()
        {
            logger = DIContainer.GetService<Log>();
            var args = DIContainer.GetService<IArgumentParser>().CommandLine;

            PathToAdminToolsConsole = Path.Combine(args.WorkingDirectory, "Akforta.eLeed.AdminToolsConsole.exe");
            PathToCompilationPlugin = Path.Combine(args.WorkingDirectory, "BIZ.Compiler.exe");
        }

        public void Run(string filename, string args)
        {
            lock(locker)
            {
                logger.Info(string.Format("{0} {1}", filename, args));

                var pinfo = GetProcessStartInfo(filename, args);
                var proc = GetProcess(pinfo);

                RegisterDataReceivedEvents(proc);
                StartProcess(proc);
                LogProcess(proc);
                EleedSpecificCheckings(filename, proc.ExitCode);
            }
        }

        private ProcessStartInfo GetProcessStartInfo(string filename, string args)
        {
            ProcessStartInfo pinfo = new ProcessStartInfo(filename, args);
            pinfo.RedirectStandardError = true;
            pinfo.RedirectStandardOutput = true;
            pinfo.UseShellExecute = false;
            return pinfo;
        }

        private Process GetProcess(ProcessStartInfo pinfo)
        {
            Process proc = new Process();
            proc.StartInfo = pinfo;
            return proc;
        }

        private void RegisterDataReceivedEvents(Process proc)
        {
            stdout = stderr = string.Empty;
            proc.OutputDataReceived += (sender, args) => stdout += args.Data + Environment.NewLine;
            proc.ErrorDataReceived  += (sender, args) => stderr += args.Data + Environment.NewLine;
        }

        private void StartProcess(Process proc)
        {
            proc.Start();
            proc.BeginOutputReadLine();
            proc.BeginErrorReadLine();

            var waitTime = (int)TimeSpan.FromHours(1).TotalMilliseconds;
            proc.WaitForExit(waitTime);
        }

        private void LogProcess(Process proc)
        {
            int exitcode = proc.ExitCode;

            logger.Info($"stdout: {stdout}");
            logger.Info($"stderr: {stderr}");
            logger.Info($"exit code: {exitcode}");
        }

        private void EleedSpecificCheckings(string filename, int exitcode)
        {
            if (filename.Equals(PathToCompilationPlugin) && exitcode != 0)
            {
                string error = $"The execution of the programm {filename} with"+
                                $" code {exitcode} was complete with exception.";
                throw new InvalidOperationException(error);
            }
        }
    }
}