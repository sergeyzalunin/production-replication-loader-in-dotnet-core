using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using ReplicLoader.CommandLine;
using ReplicLoader.DataSource;
using ReplicLoader.Files;
using ReplicLoader.Logging;
using ReplicLoader.Messages;
using ReplicLoader.Services;

namespace ReplicLoader
{
    internal class DIContainer
    {
        public static Lazy<IServiceProvider> Provider =
                            new Lazy<IServiceProvider>(() =>
                            {
                                return new DIContainer().Generate();
                            });

        public static T GetService<T>()
        {
            T result = (T)DIContainer.Provider.Value.GetRequiredService<T>();
            return result;
        }

        private ServiceProvider Generate()
        {
            var serviceProvider = new ServiceCollection()

                            //Command line arguments
                            .AddScoped<IArgumentParser, ArgumentParser>()

                            //Logging
                            .AddTransient<Log>()
                            .AddSingleton<ILoggerFactory, LoggerFactory>()
                            .AddSingleton(typeof(ILogger<>), typeof(Logger<>))
                            .AddLogging((builder) => builder.SetMinimumLevel(LogLevel.Trace))

                            //Services
                            .AddScoped<IService<ConsoleService>, ConsoleService>()
                            .AddScoped<IService<NetPipeService>, NetPipeService>()

                            //Database
                            .AddTransient<BackupMsSql>()

                            //Files
                            .AddTransient<DescriptionLoader>()
                            .AddTransient<ReplicationLoader>()

                            //Process
                            .AddTransient<ProcessExecutor>()

                            //Messages
                            .AddTransient<IMessage, MessageMailing>()

                            //Main script of replic loader
                            .AddTransient<Loader>()
                            .BuildServiceProvider();
            RegisterLogging(serviceProvider);
            return serviceProvider;
        }

        private void RegisterLogging(ServiceProvider serviceProvider)
        {
            var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();
            var commandLine = serviceProvider.GetRequiredService<IArgumentParser>().CommandLine;

            //configure NLog
            loggerFactory.AddNLog(new NLogProviderOptions
            {
                CaptureMessageTemplates = true,
                CaptureMessageProperties = true
            });

            NLog.LogManager.LoadConfiguration("nlog.config");

            NLog.LogManager.Configuration.Variables["projectName"] = commandLine.ConsoleServiceName;
            NLog.LogManager.KeepVariablesOnReload = true;
            NLog.LogManager.Configuration.Reload();
        }
    }
}