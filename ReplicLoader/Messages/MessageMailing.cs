using MailKit;
using MimeKit;
using MailKit.Net.Smtp;
using ReplicLoader.CommandLine;
using ReplicLoader.Files;
using ReplicLoader.Logging;
using System.Collections.Generic;
using System.Linq;
using System;
using System.IO;
using System.Security.Authentication;

namespace ReplicLoader.Messages
{
    internal class MessageMailing : IMessage
    {
        private Log logger;
        private IArgumentParser commanLine;

        public string Subject { get; set; }
        public string Body { get; set; }
        public bool DeleteDescriptionFile { get; set; }

        public MessageMailing()
        {
            this.logger = DIContainer.GetService<Log>();
            this.commanLine = DIContainer.GetService<IArgumentParser>();

            this.DeleteDescriptionFile = true;
            this.Subject = this.commanLine.CommandLine.Subject;
            this.Body = this.commanLine.CommandLine.Body;
        }

        public void Send()
        {
            if (HasAnyCommandLineParameters())
            {
                MimeMessage message = GetMessage();
                SendMail(message);
            }
        }

        #region Mail Message

        private MimeMessage GetMessage()
        {
            var message = new MimeMessage ();
			message.From.Add(GetSenderAddress());
			message.To.AddRange(GetRecipientAddresses());
			message.Subject = this.Subject;
			message.Body = GetMessageBody();
            return message;
        }

        private MailboxAddress GetSenderAddress()
        {
            string senderAddress = this.commanLine.CommandLine.From;

            if (string.IsNullOrEmpty(senderAddress))
            {
                string error = "Didn't entered sender email address. Use --help command";
                throw new ArgumentException(error);
            }
			MailboxAddress result = new MailboxAddress(senderAddress);
            return result;
        }

        private List<MailboxAddress> GetRecipientAddresses()
        {
            int recipientsCount = this.commanLine.CommandLine.ToEmailList.Count();
            if (recipientsCount == 0)
            {
                string error = "No one mail recipients is entered in the command line. Use --help command";
                throw new ArgumentException(error);
            }
            List<MailboxAddress> result = new List<MailboxAddress>(recipientsCount);

            foreach(string address in this.commanLine.CommandLine.ToEmailList)
            {
                result.Add(new MailboxAddress(address));
            }
            return result;
        }

        private Multipart GetMessageBody()
        {
            DescriptionLoader descLoader = DIContainer.GetService<DescriptionLoader>();
            string description = this.DeleteDescriptionFile
                                            ? descLoader.GetDescriptionContent(this.DeleteDescriptionFile)
                                            : string.Empty;

			MimePart body = new TextPart("plain")
            {
				Text = $"{this.Body}\n\n{description}"
			};

            var result = new Multipart ("mixed");
            result.Add (body);
            result.Add (GetLogFile());

            return result;
        }

        private MimePart GetLogFile()
        {
            string path = this.logger.GetCurrentLogPath();

            var result = new MimePart("text/plain", "log")
            {
                Content = new MimeContent (File.OpenRead(path), ContentEncoding.Default),
                ContentDisposition = new ContentDisposition (ContentDisposition.Attachment),
                ContentTransferEncoding = ContentEncoding.Base64,
                FileName = Path.GetFileName (path)
            };
            return result;
        }

        private bool HasAnyCommandLineParameters()
        {
            if (this.commanLine.CommandLine.SMTPServer.Trim() == string.Empty ||
                this.commanLine.CommandLine.SMTPLogin.Trim() == string.Empty ||
                this.commanLine.CommandLine.From.Trim() == string.Empty ||
                this.commanLine.CommandLine.ToEmailList.Count() == 0 ||
                this.commanLine.CommandLine.ToEmailList.All(n => n.Trim() == string.Empty))
                return false;

            return true;
        }

        #endregion Mail Message

        #region Mail Client

        private void SendMail(MimeMessage message)
        {
			using (var client = new SmtpClient())
            {
                ArgumentOptions args = this.commanLine.CommandLine;
				//accept all SSL certificates
				client.ServerCertificateValidationCallback = (s,c,h,e) => true;

				client.Connect(args.SMTPServer, args.SMTPPort, true);

				// Note: only needed if the SMTP server requires authentication
				client.Authenticate (args.SMTPLogin, args.SMTPPassword);

				client.Send (message);
				client.Disconnect (true);
			}
        }

        #endregion Mail Client
    }
}