namespace ReplicLoader.Messages
{
    internal interface IMessage
    {
        void Send();
    }
}