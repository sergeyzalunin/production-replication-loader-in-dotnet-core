using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using CommandLine;
using ReplicLoader.CommandLine;
using ReplicLoader.DataSource;
using ReplicLoader.Files;
using ReplicLoader.Logging;
using ReplicLoader.Messages;
using ReplicLoader.Services;

namespace ReplicLoader
{
    internal class Loader
    {
        private Log logger;
        private ReplicationLoader loader;
        private IService<ConsoleService> consoleService;
        private IService<NetPipeService> netpipeService;
        private BackupMsSql dataBaseBackuper;
        private ProcessExecutor procExecutor;
        private ArgumentOptions commandLineArgs;
        private IMessage message;

        public Loader(Log logger,
                    IService<ConsoleService> consoleService,
                    IService<NetPipeService> netpipeService,
                    BackupMsSql dataBaseBackuper,
                    ProcessExecutor procExecutor,
                    IArgumentParser commandLineArgs,
                    IMessage message)
        {
            this.logger = logger;
            this.consoleService = consoleService;
            this.netpipeService = netpipeService;
            this.dataBaseBackuper = dataBaseBackuper;
            this.procExecutor = procExecutor;
            this.commandLineArgs = commandLineArgs.CommandLine;
            this.message = message;
        }

        public void Load()
        {
            try
            {
                List<string> replicList = GetReplicationList();
                if (replicList.Count > 0)
                {
                    PreloadingProcesses();

                    foreach(string replication in replicList)
                    {
                        logger.Info($"The replication {replication} is loading");
                        string arguments = GetAdminToolsConsoleArguments(replication);
                        procExecutor.Run(procExecutor.PathToAdminToolsConsole, arguments);
                        File.Delete(replication);
                        logger.Info($"The repliaction file {replication} was deleted from folder");
                    }

                    PostloadingProcesses();
                }
            }
            catch(Exception ex)
            {
                SendFailedEmail(ex);
            }
        }

        private List<string> GetReplicationList()
        {
            loader = DIContainer.GetService<ReplicationLoader>();
            List<string> replicList = loader.GetReplicationFiles();
            return replicList;
        }

        private void PreloadingProcesses()
        {
            logger.Info($"Replication(s) {loader.ReplicationDirectory} is in the directory");

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                netpipeService.StopService();
                consoleService.StopService();
            }

            dataBaseBackuper.DoBackup();

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                consoleService.StartService();
            }
        }

        private string GetAdminToolsConsoleArguments(string replication)
        {
            StringBuilder result = new StringBuilder();
            result.Append("--plugin \"InnerReplicationPlugin\" ");
            result.Append($"--user \"{commandLineArgs.User}\" ");
            result.Append($"--password \"{commandLineArgs.Password}\" ");
            result.Append("--import ");
            result.Append("--nocompilation ");
            result.Append($"--file \"{replication}\" ");
            return result.ToString();
        }

        private void PostloadingProcesses()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                string arguments = GetCompilationPluginArguments();
                procExecutor.Run(procExecutor.PathToCompilationPlugin, arguments);

                netpipeService.StartService();
            }
            logger.Info($"All replications have already loaded successfully");

            SendSuccessEmail();
        }

        private string GetCompilationPluginArguments()
        {
            StringBuilder result = new StringBuilder();
            result.Append($"--user \"{commandLineArgs.User}\" ");
            result.Append($"--password \"{commandLineArgs.Password}\" ");
            return result.ToString();
        }

        private void SendSuccessEmail()
        {
            MessageMailing mailMessage = this.message as MessageMailing;
            mailMessage.Subject = $"Replication on {commandLineArgs.ProjectName} Base Completed Successfully"+
                                  $" at {DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss")}";
            mailMessage.Send();
        }

        private void SendFailedEmail(Exception ex)
        {
            string subjectMessage = $"Replication on {commandLineArgs.ProjectName} Base Failed "+
                                    $"at {DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss")}";
            logger.Error("subjectMessage", ex);

            MessageMailing mailMessage = this.message as MessageMailing;
            mailMessage.DeleteDescriptionFile = false;
            mailMessage.Subject = subjectMessage;
            mailMessage.Body = $"The replication failed with next exception: {ex.Message}"+
                                "\nSee the attached log file for details.";
            mailMessage.Send();
        }
    }
}