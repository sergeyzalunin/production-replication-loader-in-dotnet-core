namespace ReplicLoader.Services
{
    internal interface IService<T> where T: class
    {
        bool HasService();
        void StartService();
        void StopService();
    }
}