namespace ReplicLoader.Services
{
    internal class NetPipeService : ServiceWorker, IService<NetPipeService>
    {
        public bool HasService()
        {
            return HasService(CommandLine.NetPipeServiceName);
        }

        public void StartService()
        {
            StartService(CommandLine.NetPipeServiceName);
        }

        public void StopService()
        {
            StopService(CommandLine.NetPipeServiceName);
        }
    }
}