using ReplicLoader.CommandLine;
using ReplicLoader.Logging;
using ReplicLoader.Services;
using System.ServiceProcess;

namespace ReplicLoader
{
    public abstract class ServiceWorker
    {
        protected ArgumentOptions CommandLine;
        private Log logger;

        public ServiceWorker()
        {
            CommandLine = DIContainer.GetService<IArgumentParser>().CommandLine;
            logger = DIContainer.GetService<Log>();
        }

        protected bool HasService(string serviceName)
        {
            ServiceController service = GetService(serviceName);
            if (service == null)
            {
                logger.Error($"The service {serviceName} doesn't found in the system!");
                return false;
            }
            logger.Info($"The service {serviceName} is found in the system!");
            return true;
        }

        protected void StartService(string serviceName)
        {
            ServiceController service = GetService(serviceName);
            if (service != null && service.Status != ServiceControllerStatus.Running)
            {
                logger.Info($"The service {serviceName} is starting");
                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running);
                logger.Info($"The service {serviceName} has started");
            }
        }

        protected void StopService(string serviceName)
        {
            ServiceController service = GetService(serviceName);
            if (service != null && service.Status != ServiceControllerStatus.Stopped)
            {
                logger.Info($"The service {serviceName} is stoping");
                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped);
                logger.Info($"The service {serviceName} has stoped");
            }
        }

        private ServiceController GetService(string serviceName)
        {
            if (serviceName.Trim() != string.Empty)
            {
                ServiceController[] servicesList = ServiceController.GetServices();

                if (servicesList == null)
                {
                    return null;
                }

                int position = 0;
                while (position < servicesList.Length)
                {
                    ServiceController service = servicesList[position];
                    if (service.DisplayName.Equals(serviceName))
                            return service;
                    position++;
                }
            }
            return null;
        }
    }
}