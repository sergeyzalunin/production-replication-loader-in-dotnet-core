namespace ReplicLoader.Services
{
    internal class ConsoleService : ServiceWorker, IService<ConsoleService>
    {
        public bool HasService()
        {
            return HasService(CommandLine.ConsoleServiceName);
        }

        public void StartService()
        {
            StartService(CommandLine.ConsoleServiceName);
        }

        public void StopService()
        {
            StopService(CommandLine.ConsoleServiceName);
        }
    }
}