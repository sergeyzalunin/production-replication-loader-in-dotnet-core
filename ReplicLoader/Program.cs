﻿using System;
using System.Collections.Generic;
using CommandLine;
using ReplicLoader.CommandLine;
using ReplicLoader.DataSource;
using ReplicLoader.Files;
using ReplicLoader.Logging;
using ReplicLoader.Messages;
using ReplicLoader.Services;

namespace ReplicLoader
{
    class Program
    {
        static void Main(string[] args)
        {
            Loader loader = DIContainer.GetService<Loader>();
            loader.Load();
        }
    }
}
