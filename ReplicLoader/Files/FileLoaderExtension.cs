using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ReplicLoader.Files
{
    internal static class FileLoaderExtension
    {
        public static List<string> SortFilesByWriteTime(this List<string> filesPath)
        {
            var result = filesPath.OrderBy(n => File.GetLastWriteTime(n))
                                  .ToList<string>();
            return result;
        }
    }
}