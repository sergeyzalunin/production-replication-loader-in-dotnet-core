using System.Collections.Generic;
using ReplicLoader.CommandLine;

namespace ReplicLoader.Files
{
    internal class ReplicationLoader : FileLoader
    {        
        public ReplicationLoader(IArgumentParser parser) : base(parser)
        {

        }

        public List<string> GetReplicationFiles()
        {
            List<string> result = GetReplicationFiles(this.ReplicationDirectory);
            return result;
        }
        
        public List<string> GetReplicationFiles(string directory)
        {
            List<string> result = GetFiles(directory, "*.rep").SortFilesByWriteTime();
            return result;
        }
    }
}