using System;
using System.Collections.Generic;
using System.IO;
using ReplicLoader.Logging;
using ReplicLoader.CommandLine;

namespace ReplicLoader.Files
{
    internal abstract class FileLoader
    {
        protected Log logger;
        public string ReplicationDirectory { get; set; }

        public FileLoader(IArgumentParser parser)
        {
            logger = DIContainer.GetService<Log>();
            ArgumentOptions args = parser.CommandLine;
            if (string.IsNullOrEmpty(args.DatabaseName.Trim()))
                throw new ArgumentException("The Database name didn't set in command line. Use --help command");
            string executableDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string parentDirectory = Path.GetDirectoryName(executableDirectory);
            this.ReplicationDirectory = Path.Combine(parentDirectory, $"{args.DatabaseName}Replics");
            CreateDirectoryIfNotExists(this.ReplicationDirectory);
        }

        private void CreateDirectoryIfNotExists(string directory)
        {
            if (Directory.Exists(directory) == false)
                Directory.CreateDirectory(directory);
        }

        protected List<string> GetFiles(string directory, string searchPattern)
        {
            List<string> result = new List<string>();

            if (Directory.Exists(directory))
            {
                string[] files = Directory.GetFiles(directory, searchPattern, SearchOption.TopDirectoryOnly);
                result.AddRange(files);
            }
            else
                throw new DirectoryNotFoundException($"Directory not found {directory}");

            return result;
        }
    }
}