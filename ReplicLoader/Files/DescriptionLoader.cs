using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ReplicLoader.CommandLine;

namespace ReplicLoader.Files
{
    internal class DescriptionLoader : FileLoader
    {
        public DescriptionLoader(IArgumentParser parser) : base(parser)
        {

        }

        public string GetDescriptionContent(bool removeFiles)
        {
            return GetDescriptionContent(this.ReplicationDirectory, removeFiles);
        }

        public string GetDescriptionContent(string directory, bool removeFiles)
        {
            string result = string.Empty;
            try
            {
                List<string> fileList = GetFiles(directory, "*.desc").SortFilesByWriteTime();
                foreach (string descFile in fileList)
                {
                    string content = File.ReadAllText(descFile, Encoding.UTF8);
                    result += string.Format("{0}\n", content);

                    if (removeFiles)
                    {
                        File.Delete(descFile);
                        logger.Info($"The description file {descFile} has deleted from folder");
                    }
                }
            }
            catch (DirectoryNotFoundException dnfEx)
            {
                this.logger.Error("Description directory not found", dnfEx);
                return result;
            }
            catch
            {
                throw;
            }

            return result;
        }
    }
}