using System;
using System.IO;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Targets;

namespace ReplicLoader.Logging
{
    public class Log
    {
        private readonly ILogger<Log> _logger;

        public Log(ILogger<Log> logger)
        {
            _logger = logger;
        }

        public void Info(string message)
        {
            _logger.LogInformation(message);
        }

        public void Error(string message)
        {
            _logger.LogError(message);
        }

        public void Error(string message, Exception ex)
        {
            _logger.LogError(ex, message);
        }

        public string GetCurrentLogPath()
        {
            var fileTarget = (FileTarget) LogManager.Configuration.FindTargetByName("fileTarget");
            var logEventInfo = new LogEventInfo {TimeStamp = DateTime.Now}; 
            string fileName = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), 
                                           fileTarget.FileName.Render(logEventInfo));
            
            return fileName;
        }
    }
}