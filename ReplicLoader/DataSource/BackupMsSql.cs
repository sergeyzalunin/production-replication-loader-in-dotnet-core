using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using ReplicLoader.CommandLine;
using ReplicLoader.Logging;

namespace ReplicLoader.DataSource
{
    public class BackupMsSql
    {
        private Log logger;
        private ArgumentOptions commandLine;

        public BackupMsSql()
        {
            this.logger = DIContainer.GetService<Log>();
            this.commandLine = DIContainer.GetService<IArgumentParser>().CommandLine;
        }

        private IDbConnection Connection
        {
            get
            {
                if (this.commandLine.DatabaseName == string.Empty)
                {
                    string error = "The database name didn't set in command line. Use --dbname or --help command";
                    throw new ArgumentException(error);
                }
                string connectionString = GetConnectionString(7200);

                IDbConnection result = new SqlConnection(connectionString);
                return result;
            }
        }

        private string GetConnectionString(int connectionTimeout)
        {
            StringBuilder query = new StringBuilder();
            query.Append($"Server={this.commandLine.DataSource};");
            query.Append($"Database={this.commandLine.DatabaseName};");
            query.Append($"Trusted_Connection=true;");
            query.Append($"Connection Timeout={connectionTimeout}");
            return query.ToString();
        }

        public void DoBackup()
        {
            using(IDbConnection connection = Connection)
            {
                connection.Open();
                IDbCommand command = connection.CreateCommand();
                command.CommandTimeout = 55555;
                command.CommandText = GetBackupCommand();
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
                this.logger.Info("Backup database - finish");
            }
        }

        private string GetBackupCommand()
        {
            this.logger.Info($"Backup database {this.commandLine.DatabaseName} - start");

            string filename = System.IO.Path.Combine(this.commandLine.BackupPath,
                                                     this.commandLine.DatabaseName + "_ReplicLoaderAutobackup.bak");
            string compressionString = this.commandLine.UseCompression ? "COMPRESSION," : string.Empty;


            StringBuilder query = new StringBuilder();
            query.Append($"BACKUP DATABASE {this.commandLine.DatabaseName} TO  DISK = '{filename}' WITH NOFORMAT, ");
            query.Append($"INIT,  NAME = N'{this.commandLine.DatabaseName} Database Backup',");
            query.Append($"SKIP, NOREWIND, NOUNLOAD, {compressionString}  STATS = 10");

            return query.ToString();
        }
    }
}