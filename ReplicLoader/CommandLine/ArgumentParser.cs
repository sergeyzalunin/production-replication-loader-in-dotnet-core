using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CommandLine;

namespace ReplicLoader.CommandLine
{
    internal class ArgumentParser : IArgumentParser
    {
        private string[] argumentList;

        private static Lazy<ArgumentOptions> _commandLine = new Lazy<ArgumentOptions>(() => 
        {
            return  new ArgumentParser().Parse(); 
        });

        public ArgumentOptions CommandLine 
        {
            get 
            {
                return _commandLine.Value;
            }
        }

        public ArgumentParser()
        {
            this.argumentList = Environment.GetCommandLineArgs().Skip(1).ToArray();
        }

        private ArgumentOptions Parse()
        {
            ArgumentOptions result = null;         

            Parser parser = new Parser((settings) => { settings.EnableDashDash = true; });
            ParserResult<ArgumentOptions> args = parser.ParseArguments<ArgumentOptions>(argumentList)
                                                       .WithParsed(n => { result = n; } );
            
            NotParsed<ArgumentOptions> notParsedResult = args as NotParsed<ArgumentOptions>;
            if (notParsedResult != null && 
                notParsedResult.Errors.Any(n => n.StopsProcessing && n.Tag == ErrorType.HelpRequestedError))
                {
                    Parser.Default.ParseArguments<ArgumentOptions>(argumentList);
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
            if (result == null)
                throw new ArgumentException("No one arguments didn't enter on launch. Use --help command.");

            return result;
        }
    }
}