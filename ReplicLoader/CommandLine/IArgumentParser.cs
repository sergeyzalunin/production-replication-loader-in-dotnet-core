namespace ReplicLoader.CommandLine
{
    internal interface IArgumentParser
    {
        ArgumentOptions CommandLine { get; }
    }

}