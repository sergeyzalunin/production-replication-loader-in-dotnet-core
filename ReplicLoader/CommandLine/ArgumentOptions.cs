﻿using CommandLine;
using System.Collections.Generic;

namespace ReplicLoader.CommandLine
{
    public class ArgumentOptions
    {
        #region connection

        [Option('c', "cmsName", Default = "", Required = false, HelpText = "Name of console monolitic service")]
        public string ConsoleServiceName { get; set; }


        [Option('d', "workDir", Default = "", Required = false, HelpText = "Path to the working directory")]
        public string WorkingDirectory { get; set; }


        [Option('n', "netpipeName", Default = "", Required = false, HelpText = "Name of netpipe service")]
        public string NetPipeServiceName { get; set; }


        [Option('u', "user", Default = "", Required = false, HelpText = "User name with admin permisions")]
        public string User { get; set; }


        [Option('p', "password", Default = "", Required = false, HelpText = "Password for user")]
        public string Password { get; set; }

        #endregion connection

        #region mailing

        [Option("prjName", Default = "", Required = false, HelpText = "Name of project. This name will attach to email")]
        public string ProjectName { get; set; }

        [Option('t', "emailTo", Default = new[] {""}, Required = false, HelpText = "List of emails which will send message")]
        public IEnumerable<string> ToEmailList { get; set; }

        [Option('b', "body", Default = "See the attached log file for details", Required = false, HelpText = "Message body")]
        public string Body { get; set; }

        [Option('f', "from", Default = "sergey.zalunin@akforta.com", Required = false, HelpText = "From email")]
        public string From { get; set; }

        [Option('s', "subject", Default = "", Required = false, HelpText = "Subject of email")]
        public string Subject { get; set; }

        [Option("smtp", Default = "akforta.com", Required = false, HelpText = "SMTP server address")]
        public string SMTPServer { get; set; }

        [Option("port", Default = 465, Required = false, HelpText = "Port of SMTP server")]
        public int SMTPPort { get; set; }

        [Option("smtplogin", Default = "", Required = false, HelpText = "Login to SMTP server")]
        public string SMTPLogin { get; set; }

        [Option("smtppass", Default = "", Required = false, HelpText = "Password to SMTP server")]
        public string SMTPPassword { get; set; }

        #endregion mailing

        #region database options

        [Option("dbdatasource", Default = "localhost", Required = false, HelpText = "Database Data Source Name")]
        public string DataSource { get; set; }

        [Option("dbname", Default = "", Required = false, HelpText = "Database Name")]
        public string DatabaseName { get; set; }

        [Option("backuppath", Default = "", Required = false, HelpText = "Backup path")]
        public string BackupPath { get; set; }

        [Option("usecompr", Default = false, Required = false, HelpText = "Use compression on backup database")]
        public bool UseCompression { get; set; }

        #endregion database options
    }
}
